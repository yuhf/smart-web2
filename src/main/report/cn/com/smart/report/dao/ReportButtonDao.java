package cn.com.smart.report.dao;

import cn.com.smart.dao.impl.BaseDaoImpl;
import cn.com.smart.report.bean.entity.TReportButton;
import org.springframework.stereotype.Repository;

/**
 * @author 乌草坡 2019-09-01
 * @since 1.0
 */
@Repository
public class ReportButtonDao extends BaseDaoImpl<TReportButton> {
}
