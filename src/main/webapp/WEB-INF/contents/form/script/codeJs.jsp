<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="../../base/include/import-resource.jsp" %>
<link href="${pageContext.request.contextPath}/plugins/form/css/form.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<div class="container-fluid" id="form-script-table">
    <form class="form-horizontal" role="form" id="script-form">
        <input id="id" name="id" type="hidden" value="${objBean.id}"/>
        <input id="form-id" name="formId" type="hidden" value="${formId}"/>
        <input id="init-fun" name="initFun" type="hidden" />
        <input id="submit-before-fun" name="submitBeforeFun" type="hidden" />
        <input id="submit-after-fun" name="submitAfterFun" type="hidden" />
    </form>
    <div class="row">
        <div class="col-xs-12 code-container p-l-0 p-r-0">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#loaded" aria-controls="loaded" role="tab"data-toggle="tab">初始化方法</a>
                </li>
                <li role="presentation">
                    <a href="#before" aria-controls="before" role="tab"data-toggle="tab">提交前方法</a>
                </li>
                <li role="presentation">
                    <a href="#after" aria-controls="after" role="tab" data-toggle="tab">提交后方法</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="loaded">
                    <div class="panel panel-default code-panel ace-dracula">
                        <div class="method-name method-name-header">
                            <div class="ace-dracula ace_comment">
                                &nbsp;/** <br/>
                                &nbsp;&nbsp;&nbsp;* 表单加载完后执行 <br/>
                                &nbsp;&nbsp;&nbsp;**/
                            </div>
                            &nbsp;&nbsp;<span class="ace_storage ace_type">function</span>
                            &nbsp;<span class="ace_entity ace_name ace_function">formLoaded</span>
                            <span class="ace_paren ace_lparen">(</span>
                            <span class="ace_paren ace_rparen">)</span>
                            &nbsp;<span class="ace_paren ace_lparen">{</span>
                        </div>
                        <div id="loaded-editor" class="ace-editor">${objBean.initFun}</div>
                        <div class="method-name method-name-footer">
                            <span> } </span>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="before">
                    <div class="panel panel-default code-panel ace-dracula">
                        <div class="method-name method-name-header">
                            <div class="ace-dracula ace_comment">
                                &nbsp;/** <br/>
                                &nbsp;&nbsp;&nbsp;* 提交表单前执行 <br/>
                                &nbsp;&nbsp;&nbsp;* @param param 提交时的参数 <br/>
                                &nbsp;&nbsp;&nbsp;* @returns true|false 返回true表示继续提交表单；false表示阻止提交表单 <br/>
                                &nbsp;&nbsp;&nbsp;**/
                            </div>
                            &nbsp;&nbsp;<span class="ace_storage ace_type">function</span>
                            &nbsp;<span class="ace_entity ace_name ace_function">beforeFormSubmit</span>
                            <span class="ace_paren ace_lparen">(</span>
                            <span class="ace_variable ace_parameter">param</span>
                            <span class="ace_paren ace_rparen">)</span>
                            &nbsp;<span class="ace_paren ace_lparen">{</span>
                        </div>
                        <div id="before-editor" class="ace-editor">${objBean.submitBeforeFun}</div>
                        <div class="method-name method-name-footer">
                            <span> } </span>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="after">
                    <div class="panel panel-default code-panel ace-dracula">
                        <div class="method-name method-name-header">
                            <div class="ace-dracula ace_comment">
                                &nbsp;/** <br/>
                                &nbsp;&nbsp;&nbsp;* 表单提交后执行 <br/>
                                &nbsp;&nbsp;&nbsp;* @param response 提交后的返回结果 <br/>
                                &nbsp;&nbsp;&nbsp;* @param param 提交时的参数 <br/>
                                &nbsp;&nbsp;&nbsp;**/
                            </div>
                            &nbsp;&nbsp;<span class="ace_storage ace_type">function</span>
                            &nbsp;<span class="ace_entity ace_name ace_function">afterFormSubmit</span>
                            <span class="ace_paren ace_lparen">(</span>
                            <span class="ace_variable ace_parameter">response</span>,&nbsp;
                            <span class="ace_variable ace_parameter">param</span>
                            <span class="ace_paren ace_rparen">)</span>
                            &nbsp;<span class="ace_paren ace_lparen">{</span>
                        </div>
                        <div id="after-editor" class="ace-editor">${objBean.submitAfterFun}</div>
                        <div class="method-name method-name-footer">
                            <span> } </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="edit-nav-menu">
                <button id="submit-btn" type="button" class="btn btn-primary">
                    <i class="glyphicon glyphicon-ok-sign"></i>保存
                </button>
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/plugins/ace/ace.js"></script>
<script src="${pageContext.request.contextPath}/plugins/ace/ext-language_tools.js"></script>
<script src="${pageContext.request.contextPath}/plugins/ace/mode-javascript.js"></script>
<script type="text/javascript">

    var langTools = ace.require("ace/ext/language_tools");

    var editElementArray = ['loaded-editor', 'before-editor', 'after-editor'];
    var editObjArray = new Array();
    for (var i = 0; i < editElementArray.length; i++) {
        var editor = ace.edit(editElementArray[i]);
        //设置模式
        editor.session.setMode("ace/mode/javascript");
        //设置主题
        editor.setTheme("ace/theme/dracula");
        editor.setOptions({
            enableBasicAutocompletion: true,
            enableSnippets: true,
            enableLiveAutocompletion: true
        });
        editObjArray.push(editor);
    }

    $(document).ready(function () {
        var elementArray = ['loaded', 'before', 'after'];
        var height = $(window).height() - 10;
        height = height - $(".nav-tabs").outerHeight(true) - $('#loaded .method-name-header').outerHeight(true) - $('#loaded .method-name-footer').outerHeight(true);
        for (var i = 0; i < elementArray.length; i++) {
            var editHeight = height;
            if (elementArray[i] != 'loaded') {
                editHeight = editHeight - 30;
            }
            $("#" + editElementArray[i]).height(editHeight);
        }
    });

    $("#submit-btn").click(function () {
        var initFunCode = editObjArray[0].getValue();
        var submitBeforeFunCode = editObjArray[1].getValue();
        var submitAfterFunCode = editObjArray[2].getValue();
        $("#init-fun").val(initFunCode);
        $("#submit-before-fun").val(submitBeforeFunCode);
        $("#submit-after-fun").val(submitAfterFunCode);
        var formData = $("#script-form").serialize();
        utils.waitLoading("正在保存脚本...");
        $.post("form/script/saveJs", formData, function (response) {
            utils.closeWaitLoading();
            utils.showMsg(response.msg);
            if(response.result == '1') {
                window.opener = null;
                window.open('','_self');
                window.close();
            }
        })
    });
</script>

</body>
</html>